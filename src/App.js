import Amplify from 'aws-amplify'
import config from './aws-exports'
import Register from './components/Register'
import UserInfo from './components/UserInfo'
import {
  BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom'
import './App.css'

Amplify.configure(config)

function App () {
  return (
    <div className="App h-full">
      <Router>
        <Routes>
          <Route element={<Register />} path="/" />
          <Route element={<UserInfo />} path="/user-info" />
        </Routes>
      </Router>
    </div>
  )
}

export default App
