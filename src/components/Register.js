import { API } from 'aws-amplify'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'

const Register = () => {
  const navigate = useNavigate()
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [mobile, setMobile] = useState('')

  const handleSubmit = async (e) => {
    e.preventDefault()

    await API.post('userapi', '/users', {
      body: {
        firstName: firstName,
        lastName: lastName,
        mobile: mobile
      }
    }).then(() => {
      setFirstName('')
      setLastName('')
      setMobile('')
      navigate('./user-info')
    })
  }

  return (
    <div className="App h-full">
      <div className="h-full flex justify-center items-center">
        <div className="w-full max-w-md px-8 pt-6 pb-8">
          <form onSubmit={handleSubmit}>
            <div className="mb-6">
              <label htmlFor="first-name" className="block mb-2 text-sm font-medium">First Name</label>
              <input value={firstName} onChange={(e) => setFirstName(e.target.value)} type="text" id="first-name" className="bg-gray-50 border border-gray-300 text-sm rounded-lg w-full p-2.5" placeholder="John" required pattern="[a-zA-Z\u0e00-\u0e7e]*" maxLength="30" />
            </div>
            <div className="mb-6">
              <label htmlFor="last-name" className="block mb-2 text-sm font-medium">Last Name</label>
              <input value={lastName} onChange={(e) => setLastName(e.target.value)} type="text" id="last-name" className="bg-gray-50 border border-gray-300 text-sm rounded-lg w-full p-2.5" placeholder="Doe" required pattern="[a-zA-Z\u0e00-\u0e7e]*" maxLength="30" />
            </div>
            <div className="mb-6">
              <label htmlFor="mobile" className="block mb-2 text-sm font-medium">Mobile Number</label>
              <input value={mobile} onChange={(e) => setMobile(e.target.value)} type="tel" id="mobile" className="bg-gray-50 border border-gray-300 text-sm rounded-lg w-full p-2.5" placeholder="0912345678" required pattern="^0[689][0-9]*" minLength="10" maxLength="10" />
            </div>
            <button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center mt-6">Submit</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default Register
