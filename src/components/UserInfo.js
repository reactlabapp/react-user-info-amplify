import { API } from 'aws-amplify'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

const UserInfo = () => {
  const [users, setUsers] = useState([])
  useEffect(() => {
    API.get('userapi', '/users/mobile').then((fetchUsers) => {
      setUsers([...fetchUsers])
    })
  }, [])
  return (
    <div className="App h-full">
      <div className="h-full flex justify-center items-center">
        <div className="w-full max-w-md px-8 pt-6 pb-8 overflow-x-auto">
          <div className="overflow-hidden">
            <nav className="rounded-md w-full">
              <ol className="list-reset flex">
                <Link to={{
                  pathname: '/'
                }}><li className="text-blue-600 hover:text-blue-700 text-sm font-medium mb-4">&lt;&lt; Back</li></Link>
              </ol>
            </nav>
            <table className="min-w-full">
              <thead className="border-b">
                <tr>
                  <th scope="col" className="text-sm font-medium px-6 py-4">#</th>
                  <th scope="col" className="text-sm font-medium px-6 py-4 text-left">FullName</th>
                  <th scope="col" className="xs:hidden text-sm font-medium px-6 py-4">Mobile Number</th>
                </tr>
              </thead>
              <tbody>
                {users.map((user, key) =>
                  <tr key={key} className="border-b">
                    <td className="text-sm font-medium px-6 py-4 whitespace-nowrap">{key + 1}</td>
                    <td className="text-sm font-light px-6 py-4 whitespace-nowrap text-left">{user.firstName} {user.lastName}</td>
                    <td className="xs:hidden text-sm font-light px-6 py-4 whitespace-nowrap">{user.mobile}</td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}

export default UserInfo
