const AWS = require('aws-sdk')
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware')
const bodyParser = require('body-parser')
const express = require('express')

AWS.config.update({ region: process.env.TABLE_REGION })

const dynamodb = new AWS.DynamoDB.DocumentClient()

let tableName = 'userinfodb'
if (process.env.ENV && process.env.ENV !== 'NONE') {
  tableName = tableName + '-' + process.env.ENV
}

const partitionKeyName = 'mobile'
const partitionKeyType = 'S'
const sortKeyName = ''
const sortKeyType = ''
const hasSortKey = sortKeyName !== ''
const path = '/users'
const hashKeyPath = '/:' + partitionKeyName
const sortKeyPath = hasSortKey ? '/:' + sortKeyName : ''

// declare a new express app
const app = express()
app.use(bodyParser.json())
app.use(awsServerlessExpressMiddleware.eventContext())

// Enable CORS for all methods
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', '*')
  next()
})

// convert url string param to expected Type
const convertUrlType = (param, type) => {
  switch (type) {
    case 'N':
      return Number.parseInt(param)
    default:
      return param
  }
}

/********************************
 * HTTP Get method for list objects *
 ********************************/

app.get(path + hashKeyPath, function (req, res) {
  const condition = {}
  condition[partitionKeyName] = {
    ComparisonOperator: 'EQ'
  }

  try {
    condition[partitionKeyName].AttributeValueList = [convertUrlType(req.params[partitionKeyName], partitionKeyType)]
  } catch (err) {
    res.statusCode = 500
    res.json({ error: 'Wrong column type ' + err })
  }

  const queryParams = {
    TableName: tableName,
    KeyConditions: condition
  }

  dynamodb.scan(queryParams, (err, data) => {
    if (err) {
      res.statusCode = 500
      res.json({ error: 'Could not load items: ' + err })
    } else {
      res.json(data.Items)
    }
  })
})

/*****************************************
 * HTTP Get method for get single object *
 *****************************************/

app.get(path + '/object' + hashKeyPath + sortKeyPath, function (req, res) {
  const params = {}
  params[partitionKeyName] = req.params[partitionKeyName]

  try {
    params[partitionKeyName] = convertUrlType(req.params[partitionKeyName], partitionKeyType)
  } catch (err) {
    res.statusCode = 500
    res.json({ error: 'Wrong column type ' + err })
  }

  if (hasSortKey) {
    try {
      params[sortKeyName] = convertUrlType(req.params[sortKeyName], sortKeyType)
    } catch (err) {
      res.statusCode = 500
      res.json({ error: 'Wrong column type ' + err })
    }
  }

  const getItemParams = {
    TableName: tableName,
    Key: params
  }

  dynamodb.get(getItemParams, (err, data) => {
    if (err) {
      res.statusCode = 500
      res.json({ error: 'Could not load items: ' + err.message })
    } else {
      if (data.Item) {
        res.json(data.Item)
      } else {
        res.json(data)
      }
    }
  })
})

/************************************
* HTTP put method for insert object *
*************************************/

app.put(path, function (req, res) {
  const putItemParams = {
    TableName: tableName,
    Item: req.body
  }
  dynamodb.put(putItemParams, (err, data) => {
    if (err) {
      res.statusCode = 500
      res.json({ error: err, url: req.url, body: req.body })
    } else {
      res.json({ success: 'put call succeed!', url: req.url, data: data })
    }
  })
})

/************************************
* HTTP post method for insert object *
*************************************/

app.post(path, function (req, res) {
  const putItemParams = {
    TableName: tableName,
    Item: req.body
  }
  dynamodb.put(putItemParams, (err, data) => {
    if (err) {
      res.statusCode = 500
      res.json({ error: err, url: req.url, body: req.body })
    } else {
      res.json({ success: 'post call succeed!', url: req.url, data: data })
    }
  })
})

/**************************************
* HTTP remove method to delete object *
***************************************/

app.delete(path + '/object' + hashKeyPath + sortKeyPath, function (req, res) {
  const params = {}
  params[partitionKeyName] = req.params[partitionKeyName]

  try {
    params[partitionKeyName] = convertUrlType(req.params[partitionKeyName], partitionKeyType)
  } catch (err) {
    res.statusCode = 500
    res.json({ error: 'Wrong column type ' + err })
  }

  if (hasSortKey) {
    try {
      params[sortKeyName] = convertUrlType(req.params[sortKeyName], sortKeyType)
    } catch (err) {
      res.statusCode = 500
      res.json({ error: 'Wrong column type ' + err })
    }
  }

  const removeItemParams = {
    TableName: tableName,
    Key: params
  }
  dynamodb.delete(removeItemParams, (err, data) => {
    if (err) {
      res.statusCode = 500
      res.json({ error: err, url: req.url })
    } else {
      res.json({ url: req.url, data: data })
    }
  })
})

app.listen(3000, function () {
  console.log('App started')
})

module.exports = app
